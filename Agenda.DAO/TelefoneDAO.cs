﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.DAO
{
	public class TelefoneDAO
	{
		public int cdTelefone {get; set;}
		public string nmContato {get; set;}
		public string nrTelefone {get; set;}
		public string dddTelefone {get; set;}
		public int cdTipoTelefone {get; set;}
		public int cdAgenda {get; set;}
		public DateTime dtInclusao {get; set;}
		public DateTime dtAlteracao {get; set;}

		public static List<TelefoneDAO> GetAll(){
			List<TelefoneDAO> result = new List<TelefoneDAO>();

			using (var con = new SqlConnection(DbAccess.connectionString))
			{
				StringBuilder query = new StringBuilder();
				query.AppendLine(" SELECT cdTelefone, nmContato, nrTelefone, dddTelefone, cdTipoTelefone, cdAgenda, dtInclusao, dtAlteracao ");
				query.AppendLine(" FROM TB_Telefone ");

				con.Open();
				SqlCommand cmd = new SqlCommand(query.ToString(), con);
				SqlDataReader dr = cmd.ExecuteReader();
				if (dr.HasRows)
				{
					while (dr.Read())
					{
						var item = new TelefoneDAO();
						item.cdTelefone = dr.GetInt32(dr.GetOrdinal("cdTelefone"));
						item.nmContato = dr.GetString(dr.GetOrdinal("nmContato"));
						item.nrTelefone = dr.GetString(dr.GetOrdinal("nrTelefone"));
						item.dddTelefone = dr.GetString(dr.GetOrdinal("dddTelefone"));
						item.cdTipoTelefone = dr.GetInt32(dr.GetOrdinal("cdTipoTelefone"));
						item.cdAgenda = dr.GetInt32(dr.GetOrdinal("cdAgenda"));
						item.dtInclusao = dr.GetDateTime(dr.GetOrdinal("dtInclusao"));
						item.dtAlteracao = dr.GetDateTime(dr.GetOrdinal("dtAlteracao"));
						result.Add(item);
					}
				}
				dr.Close();
			}
			return result;
		}
	}
}
