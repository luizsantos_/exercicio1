﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda.DAO
{
    public class AgendaDAO
    {
        public int cdAgenda { get; set; }
        public string nmAgenda { get; set; }
        public DateTime dtInclusao { get; set; }
        public DateTime dtAlteracao { get; set; }

        public static List<AgendaDAO> GetAll()
        {
            List<AgendaDAO> result = new List<AgendaDAO>();
            using (var con = new SqlConnection(DbAccess.connectionString))
            {
                StringBuilder query = new StringBuilder();
                query.AppendLine(" SELECT cdAgenda, nmAgenda, dtInclusao, dtAlteracao ");
                query.AppendLine(" FROM TB_Agenda ");

                con.Open();
                SqlCommand cmd = new SqlCommand(query.ToString(), con);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        var item = new AgendaDAO();
                        item.cdAgenda = dr.GetInt32(dr.GetOrdinal("cdAgenda"));
                        item.nmAgenda = dr.GetString(dr.GetOrdinal("nmAgenda"));
                        item.dtInclusao = dr.GetDateTime(dr.GetOrdinal("dtInclusao"));
                        item.dtAlteracao = dr.GetDateTime(dr.GetOrdinal("dtAlteracao"));
                        result.Add(item);
                    }
                    dr.Close();
                }
                return result;
            }
        }
    }
}