﻿using Agenda.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Agenda.UI.Controllers
{
    public class AgendaController : Controller
    {
        // GET: Agenda
        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult Lista()
        {
            return View(AgendaDAO.GetAll());

        }


        public string GetAll()
        {
            return new JavaScriptSerializer().Serialize(AgendaDAO.GetAll());
        }

    }
}