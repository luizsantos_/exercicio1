﻿using Agenda.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Agenda.UI.Controllers
{
	public class TelefoneController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
		public string GetAll()
		{
			return new JavaScriptSerializer().Serialize(TelefoneDAO.GetAll());
		}
	}

}